var Vsphere = require('vsphere');
var vc = new Vsphere.Client("10.139.163.179",
        "administrator@vsphere.local", "Admin!23", false);
var expect = require('chai').expect;
var vmName;

exports.powerOnMethod = function(req, res) {

    console.log(req.params.vmname);
    // console.log(req.params.powerCmd);

    // vc.once('ready', function() {
    console.log("connected to VC");

    var powerOnVM = vc.powerOpVMByName(req.params.vmname, 'powerOn');

    powerOnVM.once('result',
                    function(powerOnResult) {
                        // ensure VM PowerOn task successfully fired
                        expect(powerOnResult[0].result['$value']).to.be.equal('success');
                        // get the Virtual Machine ManagedObjectReference
                        var vmObj = powerOnResult[0].obj;

                        vc.waitForValues(vmObj,
                                        'summary.runtime.powerState',
                                        'powerState', 'poweredOn')
                                .once('result',
                                        function(result) {
                                            // verify VM is powered on
                                            expect(result['summary.runtime.powerState']['$value']).to.be.equal('poweredOn');

                                            res.send('Your VM will be powered ON');
                                        }).once('error', function(err) {
                                    console.error(err);
                                    res.send(err[0].result.localizedMessage);
                                });
                    }).once('error', function(err) {
                console.error(err);
                res.send(err[0].result.localizedMessage);
            });

};

exports.powerOffMethod = function(req, res) {

    console.log(req.params.vmname);
    // console.log(req.params.powerCmd);

    // vc.once('ready', function() {
    console.log("connected to VC");

    // fire powerOff command
    var powerOffVM = vc.powerOpVMByName(req.params.vmname, 'powerOff');

    powerOffVM
            .once(
                    'result',
                    function(powerOffResult) {
                        // ensure VM PowerOn task successfully fired
                        expect(powerOffResult[0].result['$value']).to.be
                                .equal('success');
                        // get the Virtual Machine ManagedObjectReference
                        var vmObj = powerOffResult[0].obj;
                        vc
                                .waitForValues(vmObj,
                                        'summary.runtime.powerState',
                                        'powerState', 'poweredOff')

                                .once(
                                        'result',
                                        function(result) {
                                            // verify VM is powered on
                                            expect(result['summary.runtime.powerState']['$value']).to.be
                                                    .equal('poweredOff');

                                            res
                                                    .send('Your VM will be powered OFF');
                                        }).once('error', function(err) {
                                    console.error(err);
                                    res.send(err[0].result.localizedMessage);
                                });
                    }).once('error', function(err) {
                console.error(err);
                res.send(err[0].result.localizedMessage);
            });
};

exports.relocateVM = function(req, res) {

// get the datastore mor using dsname
    var rootFolder = vc.serviceContent.rootFolder;
    var dsMor;
    var vmMor;
    //var vc.getMORefsInContainerByTypeName( rootFolder, 'VirtualMachine', 'myVM')
    
    var nameArray = [req.params.dsname];

    vc.getMORefsInContainerByTypePropertyArray(rootFolder, 'Datastore', 'name')
        .once('error', function(err) {
            emitter.emit('error', err);
        })
        .once('result', function(result) {
            console.log(result);
            var allDsArray = result.returnval.objects;
            var dsObjArray = [];
            var processed = 0;
            var cmdRun = false;

            allDsArray.forEach(

                function(dsObj) {

                console.log(dsObj.propSet.val['$value']);

                if (nameArray.indexOf(dsObj.propSet.val['$value']) > -1) {
                    dsObjArray.push(dsObj.obj);
                    dsMor = dsObj.obj;
                    console.log("Relocating to datastore " + dsObj.propSet.val['$value']);
                
                }
/*                else{
                    console.log("Else part of Relocating to datastore " + dsObj.propSet.val['$value']);
                }*/
            });


        });

    var vmnameArray = [req.params.vmname];

    vc.getMORefsInContainerByTypePropertyArray(rootFolder, 'VirtualMachine', 'name')
        .once('error', function(err) {
            emitter.emit('error', err);
        })
        .once('result', function(result) {

            var allvmsArray = result.returnval.objects;
            var vmsObjArray = [];
            var processed = 0;
            var cmdRun = false;
            //console.log(allvmsArray.length);
            allvmsArray.forEach(function(vmObj) {

                console.log(vmnameArray);
                console.log(vmObj.propSet.val['$value']);

                if (vmnameArray.indexOf(vmObj.propSet.val['$value']) > -1) {
                    vmsObjArray.push(vmObj.obj);
                    vmMor = vmObj.obj;

                    var vmRelocateSpec = {
                        datastore: dsMor
                    };


                    var vcCmd = vc.runCommand('RelocateVM_Task', {
                        _this: vmMor,
                        spec: vmRelocateSpec
                    })
                    vcCmd.once('result', function(result, raw, soapHeader) {
                        res.send("VM will be relocated");
                    });
                    vcCmd.once('error', function(err) {
                        // handle errors
                    });




                }
            });


        });
};

exports.listAllVM = function(req, res) {

// get the datastore mor using dsname
    var rootFolder = vc.serviceContent.rootFolder;
    var dsMor;
    var vmMor;
    //var vc.getMORefsInContainerByTypeName( rootFolder, 'VirtualMachine', 'myVM')

    vc.getMORefsInContainerByTypePropertyArray(rootFolder, 'VirtualMachine', 'name')
        .once('error', function(err) {
            emitter.emit('error', err);
        })
        .once('result', function(result) {

            var allvmsArray = result.returnval.objects;
            var vmsObjArray = [];
            var processed = 0;
            var cmdRun = false;
            console.log(allvmsArray.length);

            var vmnameArray = [req.params.vmname];

        

            allvmsArray.forEach(function(vmObj) {

                console.log(vmObj.propSet.val['$value']);
                vmsObjArray.push(vmObj.propSet.val['$value']);
            });
            res.send(vmsObjArray);


       });
};
