var express = require('express');
var appExpress = express();

require('./routes')(appExpress);

appExpress.configure(function() {
	appExpress.use(express.bodyParser());
});

appExpress.listen(3001);
console.log('Listening on port 3001...');

/*
 * var vcCmd = vc.runCommand( commandToRun, arguments ); vcCmd.once('result',
 * function( result, raw, soapHeader) { // handle results });
 * vcCmd.once('error', function( err) { // handle errors });
 * 
 * var rootFolder = vc.serviceContent.rootFolder;
 * 
 * vc.getMORefsInContainerByType( rootFolder, 'VirtualMachine')
 * 
 * vc.getMORefsInContainerByTypeName( rootFolder, 'VirtualMachine', 'myVM')
 * 
 * vc.getMORefProperties( MORef ) vc.getMORefProperties( MORef, propList )
 * 
 * vc.getMORefsInContainerByTypePropertyArray( rootFolder, 'VirtualMachine',
 * ['name', 'config'])
 * 
 * vc.getVMinContainerPowerState( rootFolder ) .once('result', function( result) { /*
 * result = [{ obj: { attributes: { type: 'VirtualMachine' }, '$value': '4' },
 * name: 'testvm-win', powerState: 'poweredOff' }, ...]
 */
/*
 * }); .once('error', function( err) { // handle errors });
 * 
 * vc.powerOpVMByName( vmName, powerOp) /* vmName can be a string (for a single
 * VM) or an array of strings (for multiple VMs) powerOp is one of ['powerOn',
 * 'powerOff', 'reset', 'standby', 'shutdown', 'reboot', 'suspend']
 */

// vc.waitForValues( MORef, filterProps, endWaitProps, expectedVals)
/*
 * emits result when the specified properties of a ManagedObjectReference MORef =
 * ManagedObject to monitor filterProps = properties to filter/retrieve from
 * MORef endWaitProps = property to monitor expectedVals = values of property to
 * monitor (endWaitProps) that will trigger command to emit result
 */

/* usage example for powering on and off a VMa VM */

