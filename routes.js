module.exports = function(appExpress){


    var vcCommands = require('./controllers/vcCommands');
    appExpress.get('/vcCommands/powerCMD/powerOff/:vmname', vcCommands.powerOffMethod);
    appExpress.get('/vcCommands/powerCMD/powerOn/:vmname', vcCommands.powerOnMethod);
    appExpress.get('/vcCommands/powerCMD/listAllVM', vcCommands.listAllVM);
    appExpress.get('/vcCommands/powerCMD/relocateVM/:vmname/:dsname', vcCommands.relocateVM);
}